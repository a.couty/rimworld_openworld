FROM debian:latest

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install wget curl unzip libicu67 -y && \
    useradd -m openworldserver && \
    cd /home/openworldserver && \
    wget https://github.com/TastyLollipop/OpenWorld/releases/download/1.3.9/LinuxX64_SelfContained.zip && \
    unzip LinuxX64_SelfContained.zip && \
    rm LinuxX64_SelfContained.zip && \
    chmod +x Open\ World\ Server && \
    mv Open\ World\ Server OpenWorldServer && \
    chown -R openworldserver:openworldserver /home/openworldserver
RUN apt-get install -y gettext-base
WORKDIR /home/openworldserver
COPY start.sh start.sh
RUN chown openworldserver:openworldserver ./start.sh &&  chmod +x ./start.sh

USER openworldserver

ENV HOST_IP=0.0.0.0

ENTRYPOINT ./start.sh
